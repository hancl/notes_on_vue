# vue的笔记

1. 在vue中，子组件里面定义的自定义事件好像不起作用， **this.$emit** 只能触 **发全局的自定义事件吗** ，原谅我还没学到深层的组件嵌套
```javascript
Vue.component('my-component', {
            template: '<div @click="clickMe" @in="tell">{{message}}</div>',
            props: ["message"],
            data: function(){
                return {
                    a:1
                }
            },
            methods:{
                clickMe: function(){
                    this.$emit("in")
                },
                tell:function(){
                    alert("component")
                }
            }
        })
```
上面的子组件中的自定义事件 **@in** 就没法触发

